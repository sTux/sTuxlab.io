---
layout: page
title: About Me!!
---

Hello, World! This is Ritam Dey, also known as sTux/BangaliBabu in forums. This site will not only be for portfolio purposes but also as a centralised place to display all my works.

I will mainly write about:
- write-ups of vulnerable boxes from [_TryHackMe_](https://www.tryhackme.com), [_HackTheBox_](https://www.hackthebox.eu) and [_Vulnhub_](https://www.vulnhub.com)
- Crackmes mainly from [_crackmes.one_](https://www.crackmes.one) and other sources
- Pwn challenges mainly from [_pwnable.kr_](https://www.pwnable.kr) and [_pwnable.xyz_](https://www.pwnable.xyz)
- CTF challenges, although I'm not currently participating in them
- My hot-takes on concepts related to cyber-security, mainly reverse engineering and pwning
- And sometimes I'll also write about Android Development as that's what I pursue on the side

So, welcome to my site, I hope you've a great stay. My socials are:
- [Twitter](https://www.twitter.com/RitamDey420)
- [GitHub](https://www.github.com/RitamDey)
- [Gitlab](https://www.gitlab.com/sTux)
- [TryHackMe](https://tryhackme.com/p/BangaliBabu)
- [HackTheBox](https://app.hackthebox.eu/profile/95183)

<script src="https://tryhackme.com/badge/19835"></script>
