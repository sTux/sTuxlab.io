---
layout: post
title: Easily find IP of VMs in a Virtualbox Internal Network
category: misc
tags:
- shell
- shortcuts
- virtualbox
date: 2021-01-07 13:26 +0530
---
For quite sometime I've been Vulnhub boxes and following their suggestion about [setting-up lan network](https://www.vulnhub.com/lab/network/). But sometimes getting the IP address maybe a pain, as the MOTD scripts may not work.

In that case entire network scans using tools like **netdiscover**, which takes up un-necessary time.

But that can be easily prevented. Remember that Host-Only networks are essentially virtualized by the hypervisor and thus the IP allocation is managed by itself.

Thus we can use the hypervisor tools, here Virtualbox's `VboxManage` tools, to get the leased IP by the DHCP server

And I've done just that. The script has mainly 3 parts. Let's break it down.

### Part 1 - Getting a list of running VMs
```bash
function running_vms() {
    VBoxManage list runningvms | grep -o '".*"' | tr -d '"'
}
```
This one is pretty simple. I get the list of running VMs. The list contains the names of running VMs enclosed in `"`.
I extracted that using `grep` and then use `tr` to delete the `"`. Now I've the names of the running VMs

### Part 2 - Getting the MAC address of each running VMs
```bash
function running_vm_mac() {
    VBoxManage showvminfo "$1" --details | awk '{ print $4 }' | tr -d ','
}
```
So quite a few things is going on here. Lets unpack
- This function expects a argument, which is supposed to be a running VM name. This name is used to get the entire details of VM, which includes the Network Info
- Once the info is fetched, I use `awk` to select the 4th element in the line. Now `awk` basically splits the input line by whitespaces, and using the `$<number>` we can basically index into the array of split words. Here the actual MAC is the 4th element
- Now that I've the MAC address of the running VM, the only thing that remains is to delete the trailing *,* That's done by `tr`

And after all this, I've the MAC address of the VM. That will be used to query the Virtualbox DHCP server to find the IP leased out

### Part 3 - Querying the IP for a particular VM
```bash
function running_vm_ip() {
    VBoxManage dhcpserver findlease --interface=vboxnet0 --mac-address="$1" | awk '/IP/ { print $3 }'
}
```
Much like the previous function, this one has 2 parts
- This function expects a argument, which is supposed to be a running VM MAC Address.
- Use the `dhcpserver findlease` command is used to query Virtualbox's internal DHCP server in a particular interface. Here the interface is `vboxnet0` Host-Only network. Like every DHCP server, it will need a MAC address to actually query and find the corresponding IP address.
- The ouput is pipped into `awk` that matches for a line with *IP* pattern and extract the 3rd word from that. It's the IP address assigned to the MAC address, and by extension the running VM.


### Stiching it all together
Now let's make use of the above 3 functions to make a final function that will ease the process of fetching the IP addresses
```bash
function vm_ip() {
    # If called without arguments, shows the IP for every running VM
    # Else just show the IP of the specified VM name
    if [[ $# == 0 ]]; then
        running_vms | while read vm; do
            mac=$( running_vm_mac "$vm" )
            ip=$(running_vm_ip $mac )
            echo "$vm $ip"
        done
    else
        mac=$( running_vmc_mac "$1" )
        ip=$( running_vm_ip $mac )
        echo "$1 $ip"
    fi
}
```
This function can be used in two ways
1. Either you can call this function from the command-line with the name of specific VM. If the VM exists and uses the `vboxnet0` Host-Only network, then the function will find it's IP and echo it out.
2. If it's called without any arguments, then the function will first call the `running_vms` function to get the names of all the currently running VMs, then iterate over them using `while read` loop and for each running VM use `running_vm_mac` to get it's MAC address and then call `running_vm_ip` function with the MAC address to get it's IP address. Once done, output in a properly formatted way.


And there you have it, a simple way to get the IP addresses of VMs using a particular Virtualbox Host-Only adapter. Here's a screenshot on my machine

![Script output](/assets/res/handy-vm-ip-script/script.png)