---
layout: post
title: Hello World!
tags: introduction
---

Hello, World! This is Ritam Dey, also known as sTux/BangaliBabu in forums. For quite sometimes I wanted to host a site for not only portfolio purposes but also as a centralised place to display all my works. 
But laziness and busy schedules always prevailed. But **NO MORE!!**. This week I finally made the resolve to host my site for real.
My intial choice was Github Pages because it is relatively easy and popular but man it's a pain to host a jekyll site with decent theme. After a lot of fighting and troubleshooting, I dumped Github and here we are!!

I will mainly write about:

- write-ups of vulnerable boxes from [_TryHackMe_](https://www.tryhackme.com), [_HackTheBox_](https://www.hackthebox.eu) and [_Vulnhub_](https://www.vulnhub.com)
- Crackmes mainly from [_crackmes.one_](https://www.crackmes.one) and other sources
- Pwn challenges mainly from [_pwnable.kr_](https://www.pwnable.kr) and [_pwnable.xyz_](https://www.pwnable.xyz)
- CTF challenges, although I'm not currently participating in them
- My hot-takes on concepts related to cyber-security, mainly reverse engineering and pwning
- And sometimes I'll also write about Android Development as that's what I pursue on the side

So, welcome to my site, I hope you've a great stay. My socials can be found in the [about page](/about)
